#include "postgres.h"
#include "utils/builtins.h"
#include "funcapi.h"
#include "fmgr.h"
#include "utils/array.h"
#include "catalog/pg_type.h"
#include "catalog/namespace.h"
#include "access/relation.h"
#include "utils/rel.h"
#include "access/heapam.h"
#include "utils/snapmgr.h"

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(hello_world_warn);
PG_FUNCTION_INFO_V1(hello_world_value);
PG_FUNCTION_INFO_V1(hello_world_row);
PG_FUNCTION_INFO_V1(hello_world_arr);
PG_FUNCTION_INFO_V1(hello_param);
PG_FUNCTION_INFO_V1(hello_int);
PG_FUNCTION_INFO_V1(hello_bool);
PG_FUNCTION_INFO_V1(hello_tablename);

Datum hello_tablename(PG_FUNCTION_ARGS)
{
    Relation	rel;
    int32   relid = PG_GETARG_INT32(0);
    
    rel = relation_open(relid, AccessShareLock);
    HeapScanDesc heapScan = heap_beginscan(rel, SnapshotSelf, 0, (ScanKey) NULL,0,0);
    


Datum	values[2];
bool nulls[2];

for (;;)
{
    HeapTuple local_tuple;
    local_tuple = heap_getnext(heapScan, ForwardScanDirection);
    if (local_tuple == NULL)
        break;
        heap_deform_tuple(local_tuple, rel->rd_att, values, nulls);
    elog(WARNING,
           "%i", DatumGetInt32(values[0])
    );
}

heap_endscan(heapScan);
relation_close(rel, AccessShareLock);

    //elog(WARNING, "One");
    PG_RETURN_NULL();
}

Datum hello_bool(PG_FUNCTION_ARGS)
{

    bool   mybool = PG_GETARG_INT32(0);
        
    PG_RETURN_BOOL(!mybool);
}

Datum hello_int(PG_FUNCTION_ARGS)
{

    int32   myvar = PG_GETARG_INT32(0);
        
    PG_RETURN_INT32(myvar+myvar);
}

Datum hello_world_row(PG_FUNCTION_ARGS)
{
    FuncCallContext     *funcctx;
    int                  call_cntr;
    int                  max_calls;
    TupleDesc            tupdesc;
    AttInMetadata       *attinmeta;

    /* stuff done only on the first call of the function */
    if (SRF_IS_FIRSTCALL())
    {
        MemoryContext   oldcontext;

        /* create a function context for cross-call persistence */
        funcctx = SRF_FIRSTCALL_INIT();

        /* switch to memory context appropriate for multiple function calls */
        oldcontext = MemoryContextSwitchTo(funcctx->multi_call_memory_ctx);

        /* total number of tuples to be returned */
        funcctx->max_calls = 1;

        /* Build a tuple descriptor for our result type */
       get_call_result_type(fcinfo, NULL, &tupdesc);

        /*
         * generate attribute metadata needed later to produce tuples from raw
         * C strings
         */
        attinmeta = TupleDescGetAttInMetadata(tupdesc);
        funcctx->attinmeta = attinmeta;

        MemoryContextSwitchTo(oldcontext);
    }

    /* stuff done on every call of the function */
    funcctx = SRF_PERCALL_SETUP();

    call_cntr = funcctx->call_cntr;
    max_calls = funcctx->max_calls;
    attinmeta = funcctx->attinmeta;

    if (call_cntr < max_calls)    /* do when there is more left to send */
    {
        char       **values;
        HeapTuple    tuple;
        Datum        result;

        /*
         * Prepare a values array for building the returned tuple.
         * This should be an array of C strings which will
         * be processed later by the type input functions.
         */
        values = (char **) palloc(2 * sizeof(char *));
        values[0] = (char *) palloc(32 * sizeof(char));
        values[1] = (char *) palloc(32 * sizeof(char));


        snprintf(values[0], 10, "Hello ");
        snprintf(values[1], 10, "world!");

        /* build a tuple */
        tuple = BuildTupleFromCStrings(attinmeta, values);

        /* make the tuple into a datum */
        result = HeapTupleGetDatum(tuple);

        /* clean up (this is not really necessary) */
        pfree(values[0]);
        pfree(values[1]);
        pfree(values);

        SRF_RETURN_NEXT(funcctx, result);
    }
    else    /* do when there is no more left */
    {
        SRF_RETURN_DONE(funcctx);
    }
}

Datum hello_world_arr(PG_FUNCTION_ARGS)
{
    Datum       elements[2];
    ArrayType   *array;

    elements[0] = PointerGetDatum("Hello, ");
    elements[1] = PointerGetDatum("world!");

    //elements[0] = cstring_to_text("Hello ");
    //elements[1] = cstring_to_text("world!");

    //elements[0] = CStringGetTextDatum("Hello ");
    //elements[1] = CStringGetTextDatum("world!");
    //elog(DEBUG2,"%s",elements[0]);
    //elog(DEBUG2,"%s",elements[1]);
    // elements[0] = 111;
    // elements[1] = 222;

    array = construct_array(elements, 2, TEXTOID, -1, false, 'i');

    PG_RETURN_POINTER(array);
}


Datum hello_world_warn(PG_FUNCTION_ARGS)
{

    elog(WARNING, "Hello World");
    PG_RETURN_NULL();
}

Datum hello_world_value(PG_FUNCTION_ARGS)
{
    PG_RETURN_TEXT_P(cstring_to_text("Hello, World!"));
}

Datum hello_param(PG_FUNCTION_ARGS)
{

    text     *t = PG_GETARG_TEXT_P(0);
    text     *new_t = (text *) palloc(VARSIZE(t));

    SET_VARSIZE(new_t, VARSIZE(t));
    memcpy((void *) VARDATA(new_t),
           (void *) VARDATA(t),
           VARSIZE(t) - VARHDRSZ);
    PG_RETURN_TEXT_P(new_t);
}
