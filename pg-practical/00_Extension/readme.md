Установка:
- make USE_PGXS=1
- sudo make USE_PGXS=1 install
- make USE_PGXS=1 installcheck
- make USE_PGXS=1 clean

psql:
- CREATE EXTENSION hello_world;
- \df

В директории с 00_Extension:
- \`pg_config --pkglibdir\`/pgxs/src/test/regress/./pg_regress hello_world_create hello_world_select
