\echo Use "CREATE EXTENSION hello_world" to load this file. \quit

CREATE FUNCTION hello_tablename(int)
  RETURNS void
AS 'MODULE_PATHNAME', 'hello_tablename'
LANGUAGE C;

CREATE FUNCTION hello_int(int)
  RETURNS int
AS 'MODULE_PATHNAME', 'hello_int'
LANGUAGE C;

CREATE FUNCTION hello_bool(bool)
  RETURNS bool
AS 'MODULE_PATHNAME', 'hello_bool'
LANGUAGE C;

CREATE FUNCTION hello_world_warn()
  RETURNS void
AS 'MODULE_PATHNAME', 'hello_world_warn'
LANGUAGE C;

CREATE FUNCTION hello_world_value()
  RETURNS text
AS 'MODULE_PATHNAME', 'hello_world_value'
LANGUAGE C;

CREATE FUNCTION hello_param(text)
  RETURNS text
AS 'MODULE_PATHNAME', 'hello_param'
LANGUAGE C;

CREATE FUNCTION hello_world_arr()
RETURNS text[]
AS 'MODULE_PATHNAME', 'hello_world_arr'
LANGUAGE C;

CREATE OR REPLACE FUNCTION hello_world_row(OUT f1 text, OUT f2 text)
RETURNS SETOF record
AS 'MODULE_PATHNAME', 'hello_world_row'
LANGUAGE C;