1. `make USE_PGXS=1`

	Создаётся в т.ч. add_func.so-файл
2. `sudo make USE_PGXS=1 install`

	add_func.so копируется в libdir-директорию
3. `psql -f add_func.sql`

Либо в psql'e:

```
CREATE FUNCTION add(int, int)
  RETURNS int
AS '$libdir/add_func', 'add_ab'
LANGUAGE C STRICT;
```
4. `psql -c "SELECT add(2,3)"`
 
 ```
 add 
----
   3
(1 row)
```
